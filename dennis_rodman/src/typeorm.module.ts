import { Module } from '@nestjs/common'
import { TypeOrmModule as NestTypeOrmModule } from '@nestjs/typeorm'

@Module({
    imports: [
        NestTypeOrmModule.forRoot({
            type: 'postgres',
            host: process.env.POSTGRES_HOST || 'localhost',
            port: Number(process.env.POSTGRES_PORT || 5433),
            username: process.env.POSTGRES_USERNAME || 'admin',
            password: process.env.POSTGRES_PASSWORD || 'mytodo',
            database: process.env.POSTGRES_DATABASE || 'postgres',
            synchronize: true,
            autoLoadEntities: true,
            entities: ['dist/entities/**/*.entity.js'],
            // migrations: ['dist/db/migrations/**/*.js'],
            // cli: { migrationsDir: 'src/db/migrations' },
        })
    ]
})
export class TypeOrmModule { }