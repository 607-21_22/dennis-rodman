import { Module } from '@nestjs/common';
import { CoffeeModule } from './coffee/coffee.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoffeeShopModule } from './coffeeshop/coffeeshop.module';
import { UserModule } from './users/users.module';
@Module({
    imports: [
        UserModule,
        TypeOrmModule,
        CoffeeModule,
        CoffeeShopModule
    ],
})
export class AppModule { }
