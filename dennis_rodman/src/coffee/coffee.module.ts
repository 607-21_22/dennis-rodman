import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Coffee } from "./coffee.entity";

@Module({
    imports: [
        TypeOrmModule.forFeature([Coffee])
    ],
    controllers: [],
    providers: [],
    exports: []
})
export class CoffeeModule { }