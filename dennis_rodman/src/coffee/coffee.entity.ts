import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { CoffeeShop } from '../coffeeShop/coffeeshop.entity';

@Entity('Coffee')
export class Coffee {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    description: string;

    @ManyToOne(() => CoffeeShop, coffeeShop => coffeeShop.coffees)
    coffeeShop: CoffeeShop;
}
