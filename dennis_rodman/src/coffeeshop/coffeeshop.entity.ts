import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Coffee } from '../coffee/coffee.entity';

@Entity('CoffeeShop')
export class CoffeeShop {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    location: string;

    @OneToMany(() => Coffee, coffee => coffee.coffeeShop)
    coffees: Coffee[];
}