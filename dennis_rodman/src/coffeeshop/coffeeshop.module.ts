import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { CoffeeShop } from "./coffeeshop.entity";

@Module({
    imports: [
        TypeOrmModule.forFeature([CoffeeShop])
    ],
    controllers: [],
    providers: [],
    exports: []
})
export class CoffeeShopModule { }