import asyncio
import httpx
from fastapi import FastAPI

app = FastAPI()

async def fetch_data_from_api(url):
    async with httpx.AsyncClient() as client:
        try:
            response = await client.get(url)
            if response.status_code == 200:
                return response.json()
            else:
                return None
        except httpx.HTTPError as e:
            print(f"HTTP error occurred: {e}")
            return None

@app.get("/get_data")
async def get_data_from_api(api_url: str):
    data = await fetch_data_from_api(api_url)
    if data:
        return {"success": True, "data": data}
    else:
        return {"success": False, "message": "Failed to fetch data from the API"}

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000)
#http://127.0.0.1:8000/get_data?api_url=https://api.publicapis.org/entries
